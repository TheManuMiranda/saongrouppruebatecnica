﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaonGroupPruebaTecnica.Entities
{
    public abstract class BoardPublication
    {
        /*Adding abstract class for it to be highly maintainable, 
         * therefore the application can grow to have different 
         * types of board publications - Only common functionality 
         * will be placed in this class*/

        [Key]
        public int Id { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Date published")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Expires")]
        public DateTime ExpiresAt { get; set; }
    }
}
