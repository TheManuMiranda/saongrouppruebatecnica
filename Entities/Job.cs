﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaonGroupPruebaTecnica.Entities
{
    public class Job : BoardPublication
    {
        [Display(Name = "Job Name")]
        public string JobName { get; set; }
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }
        public Job()
        {
        }
    }

   
}
