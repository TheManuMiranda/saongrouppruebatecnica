﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaonGroupPruebaTecnica.Entities
{
    public class JobDBContext: DbContext
    {
        public DbSet<Job> Job { get; set; }
        
        public JobDBContext()
        {
        }

        public JobDBContext(DbContextOptions options) : base(options)
        {
        }
    }
}
